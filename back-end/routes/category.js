const express = require('express')
const router = express.Router()
const auth = require('../auth')
const CategoryControllers = require('../controllers/category')


//For ADDING categories
router.post('/add-category', auth.verify, (req,res) => {
	const params ={
		userId: auth.decode(req.headers.authorization).id,
	}
	
	CategoryControllers.addCategory(req.body).then(result => res.send(result))
})


//For getting all the category types
router.get('/getAllType',auth.verify, (req,res) => {
	const params ={
		userId: auth.decode(req.headers.authorization).id,
	}
	CategoryControllers.getAllType().then(category => res.send(category))
})

//For getting all the income Names of the user
router.get('/getAllIncome', auth.verify, (req,res) => {
	const params ={
		userId: auth.decode(req.headers.authorization).id,
	}

	CategoryControllers.getAllIncome().then(category => res.send(category))
})

//For getting all the Expense Names of the user
router.get('/getAllExpense',auth.verify, (req,res) => {
	const params ={
		userId: auth.decode(req.headers.authorization).id,
	}
	CategoryControllers.getAllExpense().then(category => res.send(category))
})







module.exports = router

