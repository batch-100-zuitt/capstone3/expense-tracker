const express = require('express')
const router = express.Router()
const auth = require('../auth')
const RecordController = require('../controllers/record')


router.post('/add-record',auth.verify, (req,res) => {
	const params ={
		userId: auth.decode(req.headers.authorization).id,
	}
	RecordController.addRecord(req.body).then(result => res.send(result))
})

module.exports = router