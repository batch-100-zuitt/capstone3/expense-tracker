const express = require('express')
const router = express.Router()
const auth = require('../auth')
const UserController = require('../controllers/user')



router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result))   
})

//Routes for user Registration
router.post('/register', (req, res) => {
    UserController.register(req.body).then(result => res.send(result))
})

//Routes for user Log In
router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result))
})


//for getting details of user
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(user => res.send(user))
})

//for verifying the google token
router.post('/verify-google-id-token', async (req, res) => {
    res.send( await UserController.verifyGoogleTokenId(req.body.tokenId))
})

module.exports = router
