const Category = require('../schema/category')
const Record = require('../schema/record');
const User = require('../schema/user')


//For adding Category
module.exports.addCategory = (params) => {
	let category = new Category({
		categoryName: params.categoryName,
		categoryType: params.categoryType,
		userId: params.userId

	})

	return category.save().then((category, err) => {
			return (err) ? false : true
	})
}



//get all category Types
module.exports.getAllType = () =>{
		return Category.find( {categoryType: ["Income","Expense"]}).then(categoryType => categoryType)
}

//For getting all the name in Income category
module.exports.getAllIncome = () =>{
		return Category.find( {categoryType: "Income"}).then(categoryIncome => categoryIncome)
}
//For getting all the name in Expense category
module.exports.getAllExpense = () =>{
		return Category.find( {categoryType: "Expense"}).then(categoryExpense => categoryExpense)
}

module.exports.getExpense = () =>{
		return Category.find( {categoryType: "Expense"}).then(categoryExpense => categoryExpense)
}










//Incase need

/*
Post.findOne({_id: 123})
.populate('postedBy')
.exec(function(err, post) {
    // do stuff with post
}); 
*/