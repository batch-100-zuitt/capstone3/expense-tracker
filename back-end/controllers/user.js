const User = require('../schema/user')
const auth = require('../auth')
const bcrypt = require('bcryptjs')

//for google log in
const clientId = '729879521042-bg9i7i2jsged8a1li99vc54b7c6surb8.apps.googleusercontent.com'
const { OAuth2Client } = require('google-auth-library');

module.exports.emailExists = (params) => {
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}


//for Register User
module.exports.register = (params) =>{
	let user = new User({

		firstName: params.firstName,
		 lastName: params.lastName,
		    email: params.email,
		 mobileNo: params.mobileNo,
		 password: bcrypt.hashSync(params.password,10)

	})

	return user.save().then((user,err) => {
			return (err) ? false: true
	})
}

//for Login User
module.exports.login = (params) => {
	return User.findOne({ email:params.email }).then(user => {

		if (user === null) { return false }

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)


		if (isPasswordMatched) {
			 return { accessToken: auth.createAccessToken(user.toObject()) }
		} else {
			return false
		}
	})
}

//for getting details of user
module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}





//for Google Log in
module.exports.verifyGoogleTokenId = async (tokenId) => {
	

	const client = new OAuth2Client(clientId);
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	})

	console.log(data.payload.email_verified);

	if(data.payload.email_verified === true){

		const user = await User.findOne({ email: data.payload.email });

	
		if (user !== null){
			
			 if(user.loginType === "google"){
			 	return { accessToken: auth.createAccessToken(user.toObject()) };
	
			 } else {
			 	return {error: "login-type-error"}
			 }

		} else {

			let user = new User({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google'
			})

			return user.save().then((user, err) => {
				return { accessToken: auth.createAccessToken(user.toObject()) };
			})

		}
	} else {

		return { error: "google-auth-error"}
	}



};