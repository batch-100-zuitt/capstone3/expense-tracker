const mongoose = require('mongoose')


const recordSchema = new mongoose.Schema({
	amount: {
		  type: Number,
	  required: [true, 'Amount is Required']
	},
	description:{
		   type: String,
	   required: [true, 'Description is Required']  
	},	
	recordBalance:{
			type: Number,	
			required: [true, 'Description is Required']   
	},
	type:{
			type: String,
            enum: ["Income","Expense"],
            required : true
	    },
	userId:{
			type: String,
			required:[true, 'User ID is required']
	},

	createdOn: {
		type: Date,
		default: new Date()
	}
	
			
		


});

module.exports = mongoose.model ('record',recordSchema)











