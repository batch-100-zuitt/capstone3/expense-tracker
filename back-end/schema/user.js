const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	firstName: {
		    type: String,
		required: [true, 'First name is required.']
	},
	lastName: {
		    type: String,
		required: [true, 'First name is required.']
	},
	email: {
			type: String,
		required: [true, 'Email is required.']
	},

	mobileNo:{
			type: String,
		
	},

	password:{
		    type: String,
		
	},
	loginType:{
		    type: String,
		    default: "Email"
		
	},
	balance:{
		    type: Number,
			default: 0
	}
	

	
})

module.exports = mongoose.model('user',userSchema)