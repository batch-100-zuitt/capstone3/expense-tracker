const mongoose = require('mongoose')

const categorySchema = new mongoose.Schema({
	 categoryName:{
			 type: String,
		 required: [true, "Category Name is Required"]
	},
	categoryType:{
			type: String,
            enum: ["Income","Expense"],
            required : true
	    },
	
	userId:{
			type: String,
			required:[true, 'User ID is required']
	},
	createdOn:{
		type: Date,
		default: new Date()
	}

		
	  

	    		
	 	
});

module.exports = mongoose.model('category',categorySchema);