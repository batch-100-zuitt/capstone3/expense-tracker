import React, { useContext } from 'react';
// Import nextJS Link component for client-side navigation
import Link from 'next/link';
// Import necessary bootstrap components
import {Navbar,Nav} from 'react-bootstrap';

import UserContext from '../UserContext';

export default function NavBar() {
    // Consume the UserContext and destructure it to access the user state from the context provider
    const { user } = useContext(UserContext);

    return (
        <Navbar bg="light" expand="lg">
            <Link href="/">
                <a className="navbar-brand">Budget Tracker</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                     
                 {(user.id !== null)
                    ?
                       <React.Fragment>  
                            <Link href="/category">
                                <a className="nav-link" role="button">
                                    Categories
                                </a>
                            </Link> 
                            <Link href="/">
                                <a className="nav-link" role="button">
                                    Records
                                </a>
                            </Link>
                            <Link href="/">
                                <a className="nav-link" role="button">
                                    Monthly Expense
                                </a>
                            </Link>
                            <Link href="/">
                                <a className="nav-link" role="button">
                                    Monthly Income
                                </a>
                            </Link>
                            <Link href="/">
                                <a className="nav-link" role="button">
                                    Trend
                                </a>
                            </Link>
                            <Link href="/logout">
                                <a className="nav-link" role="button">
                                    Logout
                                </a>
                            </Link> 

                        </React.Fragment>     
                            
                            :
                        <React.Fragment>      
                           <Link href="/register">
                                <a className="nav-link" role="button">
                                    Register
                                </a>
                            </Link> 

                           <Link href="/login">
                                <a className="nav-link" role="button">
                                    LogIn
                                </a>
                            </Link>    

                        </React.Fragment>    
                    }
                    

                </Nav>
            </Navbar.Collapse>
        </Navbar>

    )
}
