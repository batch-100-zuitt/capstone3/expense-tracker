import { useState, useEffect,useContext } from 'react';
import { Form,Button,Dropdown } from 'react-bootstrap';
import UserContext from '../../UserContext';
import AppHelper from '../../app-helper';

export default function Index(  ){

	const { user } = useContext(UserContext);
	const [categoryName,setCategoryName] = useState('');
	const [categoryType, setCategoryType] = useState('');
	//const [userId,setUserId] = useState(user);
	const [isActive, setIsActive] = useState(false);
	
     
	


	function addCategories(e){
		e.preventDefault();

			console.log(user);
			console.log(localStorage.getItem('token'))
		fetch(`http://localhost:4000/api/category/add-category`,  {
			method: 'POST',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'  
			},
			body:JSON.stringify({	
			  userId: user.id, 		
			 categoryName: categoryName,
			 categoryType: categoryType
			
			})

		})
		.then(res => res.json())
		.then(data => {
			
			console.log(data);
	
		if(data){
			setCategoryName('categoryName');
			setCategoryType('categoryType');
			
			alert('Category Added');
		}

	})
		setCategoryName('');
		setCategoryType('');

	}

	useEffect(() => {
		if(categoryName !== '' && categoryType !== '' ){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
			}, [categoryName, categoryType]);

	return(


		<Form onSubmit = {e => addCategories(e)} className= 'col-lg-4 offset-lg-4 my-5'>

			<Form.Group>
				<Form.Label><h4>Add Category Name</h4></Form.Label>
				<Form.Control 
					className= ' my-3'
					type="text"
					placeholder="Enter Category Name"
					value={categoryName}
					onChange={e => setCategoryName(e.target.value)}
					required/>

				<Form.Label><h4>Add Category Type</h4></Form.Label>

				<Form.Control as="select" placeholder="Select Type"
					value={categoryType}
					onChange={e => setCategoryType(e.target.value)} required custom>
				      <option>Income</option>
				      <option>Expense</option>				      
				</Form.Control>

			</Form.Group>

			{isActive
					?
					<Button 
						className="bg-primary"
						type="submit"
						id="submitBtn"
						className ="col-lg-4 offset-lg-2 my-9"
						>
						Submit
					</Button>
					:
					<Button 
						className="bg-danger col-lg-4 offset-lg-3 my-4"
						type="submit"
						id="submitBtn"
						disabled
						>
						Submit
					</Button>
				}
		
		</Form>	
	)

	
};

