import '../styles/globals.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import NavBar from '../components/NavBar';
import { UserProvider } from '../UserContext';
import AppHelper from '../app-helper';



	export default function MyApp({ Component, pageProps }) {
		//state hook for user state, define here for global scope
	  const [user, setUser] = useState({
	    email: null,
	    isAdmin: null
	  })


	  const unsetUser = () => {
        localStorage.clear();
        // Set the user global scope in the context provider to have its email set to null
        setUser({
            id: null,
            isAdmin: null
        });
    }


	useEffect(() => {
        const options = {
            headers: { Authorization: `Bearer ${ AppHelper.getAccessToken() }` }
        }

        fetch(`${ AppHelper.API_URL }/users/details`, options)
        .then(AppHelper.toJSON)
        .then((data) => {
            if (typeof data._id !== 'undefined') {
                setUser({ id: data._id })
            } else {
                setUser({ id: null })   
            }
        })
    }, [user.id])

	   //effect hook for testing the setUser() functionality
	  useEffect(() => {
	    console.log(user.id);
	  }, [user.id])
	  




	  return (
	  	<React.Fragment>
	  		<UserProvider value={{user, setUser, unsetUser}}>
	  		<Container>	
	  			<NavBar user= {user}/>
	  			<Component {...pageProps} />
	  		</Container>
	  	 	</UserProvider>
	  	</React.Fragment>
	  	);
	}

	
