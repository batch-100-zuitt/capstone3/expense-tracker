import React from 'react';
import Head from 'next/head';
import Banner from '../components/Banner';
import styles from '../styles/Home.module.css';


export default function Home() {

  const data = {
    title: "Zuitt Coding Bootcamp",
    content: "Opportunities for everyone, everywhere",
    destination: "/course",
    label: "Enroll now!"
  }



  return (
        <React.Fragment>
            <Banner dataProp={data} />
        </React.Fragment>         
  ) 
}
